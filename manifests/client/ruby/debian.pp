# debian ruby client
class mysql::client::ruby::debian {
  if versioncmp($::operatingsystemmajrelease,'9') >= 0 {
    $package = 'ruby-mysql2'
  } else {
    $package = 'libmysql-ruby'
  }
  ensure_packages([ $package ])
}
